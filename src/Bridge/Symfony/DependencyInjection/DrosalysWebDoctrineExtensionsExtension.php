<?php

/*
 * This file is part of the doctrine-extensions package.
 *
 * (c) Benjamin Georgeault
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace DrosalysWeb\DoctrineExtensions\Bridge\Symfony\DependencyInjection;

use DrosalysWeb\DoctrineExtensions\Subscriber\TableNamespaceNamingSubscriber;
use Symfony\Component\DependencyInjection\ContainerBuilder;
use Symfony\Component\DependencyInjection\Definition;
use Symfony\Component\DependencyInjection\Extension\Extension;

/**
 * Class DrosalysWebDoctrineExtensionsExtension
 *
 * @author Benjamin Georgeault
 */
class DrosalysWebDoctrineExtensionsExtension extends Extension
{
    public function load(array $configs, ContainerBuilder $container): void
    {
        $configuration = new Configuration();
        $config = $this->processConfiguration($configuration, $configs);

        if ($config['table_prefix']['enabled']) {
            if (!empty($config['table_prefix']['namespace_naming'])) {
                $definition = new Definition(TableNamespaceNamingSubscriber::class, [
                    $config['table_prefix']['namespace_naming']
                ]);

                $definition
                    ->setPublic(false)
                    ->addTag('doctrine.event_subscriber')
                ;

                $container->setDefinition(TableNamespaceNamingSubscriber::class, $definition);
            }

            if (!empty($prefixes = $config['table_prefix']['simple_prefix'])) {
                $container->setParameter('drosalys_doctrine_extensions.table_prefix.simple_prefix.namespaces', $prefixes);
            }
        }
    }

    public function getAlias(): string
    {
        return 'drosalys_doctrine_extensions';
    }
}
