<?php

/*
 * This file is part of the doctrine-extensions package.
 *
 * (c) Benjamin Georgeault
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace DrosalysWeb\DoctrineExtensions\Bridge\Symfony\DependencyInjection\Compiler;

use DrosalysWeb\DoctrineExtensions\Subscriber\TablePrefixSubscriber;
use Symfony\Component\DependencyInjection\Compiler\CompilerPassInterface;
use Symfony\Component\DependencyInjection\ContainerBuilder;
use Symfony\Component\DependencyInjection\Definition;

/**
 * Class SimplePrefixPass
 *
 * @author Benjamin Georgeault
 */
class SimplePrefixPass implements CompilerPassInterface
{
    public function process(ContainerBuilder $container): void
    {
        if (!$container->hasParameter('drosalys_doctrine_extensions.table_prefix.simple_prefix.namespaces')) {
            return;
        }

        if (empty($prefixes = $container->getParameter('drosalys_doctrine_extensions.table_prefix.simple_prefix.namespaces'))) {
            return;
        }

        foreach ($prefixes as $prefix => $namespaces) {
            $definition = new Definition(TablePrefixSubscriber::class, [
                $prefix,
                $namespaces,
            ]);

            $definition
                ->setPublic(false)
                ->addTag('doctrine.event_subscriber')
            ;

            $container->setDefinition(TablePrefixSubscriber::class . '.' . $prefix, $definition);
        }
    }
}
