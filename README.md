Doctrine Extensions
===================

A library to extend [Doctrine ORM](https://www.doctrine-project.org/projects/doctrine-orm/en/2.7/index.html)
capabilities.

## Install

```
composer require drosalys-web/doctrine-extensions
```

### Symfony integration (^4.4|^5.0)

Flex recipe not yet available.

- Enable the bundle in your `config/bundle.php`:
```php
<?php

return [
    //...
    DrosalysWeb\DoctrineExtensions\Bridge\Symfony\DrosalysWebDoctrineExtensionsBundle::class => ['all' => true],
    //...
];
```

- Create a new `config\packages\drosalys_doctrine_extensions.yaml` file, you will config the bundle here.
```yaml
drosalys_doctrine_extensions: ~
```

- That all for default configuration.

## Documentations

Here all capabilities documentations:
- [Available functions](doc/functions.md): Provide some useful Doctrine ORM functions for mariaDB.
- Auto-prefix table names (DOC TODO)

## TODO

- Better documentation.
- Flex recipe for Symfony.
- Add example for standalone usage.

## License

This bundle is under the MIT license. See the complete license:

    LICENSE
